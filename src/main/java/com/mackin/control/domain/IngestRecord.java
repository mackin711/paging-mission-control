package com.mackin.control.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IngestRecord {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");

    private LocalDateTime timestamp;
    private long satelliteId;

    private int redHighLimit;
    private int redLowLimit;

    private int yellowHighLimit;
    private int yellowLowLimit;

    private float rawValue;

    private String component;

    /**
     * constructs a IngestRecord based on a list of string components expected in the order of the expected input:
     *
     * timestamp|satellite-id|red-high-limit|yellow-high-limit|yellow-low-limit|red-low-limit|raw-value|component
     *
     * @param components list of components
     */
    public IngestRecord(String[] components) {
        this.timestamp = LocalDateTime.parse(components[0], FORMATTER);
        this.satelliteId = Long.parseLong(components[1]);
        this.redHighLimit = Integer.parseInt(components[2]);
        this.yellowHighLimit = Integer.parseInt(components[3]);
        this.yellowLowLimit = Integer.parseInt(components[4]);
        this.redLowLimit = Integer.parseInt(components[5]);
        this.rawValue = Float.parseFloat(components[6]);
        this.component = components[7];
    }

    public String getComponent() {
        return component;
    }

    public long getSatelliteId() {
        return satelliteId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public int getRedLowLimit() {
        return redLowLimit;
    }

    public float getRawValue() {
        return rawValue;
    }

    public int getRedHighLimit() {
        return redHighLimit;
    }

    public boolean underRedLowLimit() {
        return rawValue < redLowLimit;
    }
    public boolean exceedsRedHighLimit () {
        System.out.println(rawValue + " > " + redHighLimit);
        return rawValue > redHighLimit;
    }
}
