package com.mackin.control.domain;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AlertMessage {

    @JsonProperty("satelliteId")
    private long satelliteId;

    @JsonProperty("severity")
    private String severity;

    @JsonProperty("component")
    private String component;

    @JsonProperty("timestamp")
    private LocalDateTime timestamp;

    public AlertMessage(Long id, String severity, String component, LocalDateTime timestamp) {
        this.satelliteId = id;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }
}
