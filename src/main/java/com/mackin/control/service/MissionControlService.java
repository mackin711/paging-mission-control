package com.mackin.control.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mackin.control.domain.AlertMessage;
import com.mackin.control.domain.IngestRecord;

public class MissionControlService {
    public List<AlertMessage> handle(File file) throws IOException {
        List<IngestRecord> batteryRecords;
        List<IngestRecord> thermRecords;
        try (Stream<String> stream = Files.lines(Paths.get(file.getAbsolutePath()))) {
            List<IngestRecord> ingestRecords = stream.map(line -> {
               String[] components = line.split("\\|");
               return new IngestRecord(components);
            }).collect(Collectors.toList());

            batteryRecords = ingestRecords.stream().filter(record -> "BATT".equals(record.getComponent()))
                    .collect(Collectors.toList());
            thermRecords = ingestRecords.stream().filter(record -> "TSTAT".equals(record.getComponent()))
                    .collect(Collectors.toList());
        }

        List<AlertMessage> output = new ArrayList<>();
        if (!batteryRecords.isEmpty()) output.addAll(checkBatteryRecords(batteryRecords));
        if (!thermRecords.isEmpty()) output.addAll(checkThermostatRecords(thermRecords));

        return output;
    }

    private List<AlertMessage> checkThermostatRecords(List<IngestRecord> thermRecords) {
        List<AlertMessage> output = new ArrayList<>();

        // organize the satellites by their ID
        Map<Long, List<IngestRecord>> recordsBySatellite = thermRecords.stream()
                .collect(Collectors.groupingBy(IngestRecord::getSatelliteId, Collectors.toList()));

        for (Long id: recordsBySatellite.keySet()) {
            List<IngestRecord> records = recordsBySatellite.get(id);
            for (int i = 0; i < records.size(); i++) {
                // break early if there's not enough alarms left
                if (i + 3 > records.size()) break;
                IngestRecord first = records.get(i);
                IngestRecord second = records.get(i + 1);
                IngestRecord third = records.get(i + 2);

                // are the alarms within 5 minutes of each other?
                if (first.getTimestamp().isAfter(second.getTimestamp().minusMinutes(5)) &&
                    first.getTimestamp().isAfter(third.getTimestamp().minusMinutes(5))) {

                    // if all the records exceed the high limit
                    if (first.exceedsRedHighLimit() && second.exceedsRedHighLimit() && third.exceedsRedHighLimit()) {
                        output.add(new AlertMessage(id, "red", first.getComponent(), third.getTimestamp()));
                    }
                }
            }
        }

        return output;
    }

    private List<AlertMessage> checkBatteryRecords(List<IngestRecord> batteryRecords) {
        List<AlertMessage> output = new ArrayList<>();

        // organize the satellites by their ID
        Map<Long, List<IngestRecord>> recordsBySatellite = batteryRecords.stream()
                .collect(Collectors.groupingBy(IngestRecord::getSatelliteId, Collectors.toList()));

        for (Long id: recordsBySatellite.keySet()) {
            List<IngestRecord> records = recordsBySatellite.get(id);
            for (int i = 0; i < records.size(); i++) {
                // break early if there's not enough alarms left
                if (i + 3 > records.size()) break;
                IngestRecord first = records.get(i);
                IngestRecord second = records.get(i + 1);
                IngestRecord third = records.get(i + 2);

                // are the alarms within 5 minutes of each other?
                if (first.getTimestamp().isAfter(second.getTimestamp().minusMinutes(5)) &&
                        first.getTimestamp().isAfter(third.getTimestamp().minusMinutes(5))) {

                    // if all the records exceed the high limit
                    if (first.underRedLowLimit() && second.underRedLowLimit() && third.underRedLowLimit()) {
                        output.add(new AlertMessage(id, "red", first.getComponent(), third.getTimestamp()));
                    }
                }
            }
        }

        return output;
    }
}
