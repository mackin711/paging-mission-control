package com.mackin.control;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mackin.control.domain.AlertMessage;
import com.mackin.control.service.MissionControlService;

public class Main {

    /** for use as a singleton (created on first call to {@link #getMapper()}) */
    private static ObjectMapper mapper;

    public static void main(String[] args) {
        File file = getFileFromArgs(args);

        // simply execute the service's "handle" method once for the passed argument and end the program execution
        try {
            List< AlertMessage> messages = new MissionControlService().handle(file);

            // print the messages
            System.out.println(getMapper().writeValueAsString(messages));
        } catch (JacksonException e) {
            System.err.println("Failed to write output to STDOUT");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Failed to handle file IO");
            e.printStackTrace();
        }
    }

    private static ObjectMapper getMapper () {
        if (mapper == null) {
            mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
        }

        return mapper;
    }


    /**
     * get a file from the first item from passed args
     * @param args command line args of the program
     * @return the file from arguments
     * @throws RuntimeException if the file cannot be determined
     */
    private static File getFileFromArgs (String[] args) {
        if (args.length != 1) {
            throw new RuntimeException("Must pass a file as the first input");
        }

        System.out.println("Handling File: " + args[0]);
        File file = new File(args[0]);
        if (!file.exists()) {
            throw new RuntimeException("File: " + args[0] + " does not exist!");
        }

        return file;
    }

}