package com.mackin.control;


import java.io.File;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

public class MainTest {

    @Test
    public void executionTest () {
        final File file = new File(getClass().getClassLoader().getResource("input.txt").getFile());
        Assert.assertTrue(file.exists());
        Main.main(new String[] { file.getAbsolutePath() });
    }

    @Test(expected = RuntimeException.class)
    public void noArgTest () {
        Main.main(new String[0]);
    }

    @Test(expected = RuntimeException.class)
    public void invalidFileTest () {
        String file = UUID.randomUUID().toString();
        Assert.assertFalse(new File(file).exists());
        Main.main(new String[] { file } );
    }
}